/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.midteama;

/**
 *
 * @author Satit Wapeetao
 */
public class TestAnimal {
    public static void main(String[] args) {
        Cat cat = new Cat("Tom","Prank");
        System.out.println(cat.name+" behavior is "+cat.character);
        Rat rat = new Rat("Jerry","timid");
        System.out.println(rat.name+" behavior is "+rat.character);
    }
}
